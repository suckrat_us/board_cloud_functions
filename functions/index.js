const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp({
    credential: admin.credential.applicationDefault()
  });
const db = admin.firestore();

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

let generateRandomName = function(length) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
};


exports.createNote = functions.https.onRequest(async (request, response) => {

    // request.body()
    let parsed = JSON.parse(request.body);

    console.log(typeof request.body);
    console.log(typeof parsed);

    let data = parsed.data;

    console.log(data.date)
    console.log(data.user_id);
    console.log(data.user_name);
    console.log(data.note_title);
    console.log(data.note_content);

    let date = new Date(data.date);
    let date_unix = Math.round(date.getTime() / 1000);
    
    console.log(date);
    console.log(date_unix);
    let note_id = generateRandomName(20);

    let newNote = {
        note_id: note_id,
        note_title: data.note_title,
        note_content: data.note_content,
        user_id: data.user_id,
        date: data.date,    
    }

    console.log(date_unix);

    let resp = await db.collection('notes').add(newNote)
    //     .add(date_ob.getTime())
    //     .create(newNote)
        .then((result) => {
            console.log(result);
            return {type: 'success', key: 'CREATE_SUCCESS'};
        })
    //     .catch((error) => {
    //         console.error(error);
    //         return {type: 'error', key: 'CREATE_ERROR'};
    //     });

    response.send({response: 'resp'});
});

exports.addUser = functions.https.onRequest(async (request, response) => {

    response.send({response: 'resp'});
});

exports.checkUser = functions.https.onRequest(async (request, response) => {
    console.log(request.body);

    let parsed = JSON.parse(request.body);
    console.log(parsed);

    
    let data = parsed.data;

    console.log(data.id);

    let message;

    let resp = await db.collection('users')
        .where('id', '==', data.id)
        .get()
        .then(async snapshot => {
            console.log('empty', snapshot.empty);
            if(snapshot.empty) {
                return await db.collection("users")
                .add(data)
                .then((result) => {
                    console.log(result.id);
                    return 'User ${result.id} created';
                })
                .catch((error) => {
                    console.error(error);
                    return 'error';
                })
            } else {
                console.log(snapshot.docs[0].id);
                return snapshot.docs[0].id;
            }
    })
    console.log(resp);
    response.send({response: message});
});

exports.getNotes = functions.https.onRequest(async (request, response) => {

    let returnData = [];

    let note_list = [];

    await db.collection('notes')
        .get()
        .then(result => {
            result.forEach(doc => {
                console.log('document => ${doc.data()}');
                note_list.push(doc.data());
            })
            return true;
        });



        async function asyncForEach(array, callback) {
            for (let index = 0; index < array.length; index++) {
              await callback(array[index], index, array);
            }
          }
          
        const start = async (list) => {
            await asyncForEach(list, async (note) => {
              console.log(note);
                let user = await db.collection('users')
                .where('id', '==', note.user_id).get().then((userData) => {
                    console.log('user data => ' +userData);
                    return userData;
                });
                let dataMap = {
                    user: user,
                    data: note,
                }

                console.log('DATA =>' +dataMap);

                returnData.push(dataMap);
            });
            console.log('Done');
          }

        start(note_list);

        console.log(returnData);
    response.send({response:  returnData});
});

exports.editNote = functions.https.onRequest(async (request, response) => {

    response.send({response: 'resp'});
});
